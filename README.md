## NER Error Daily Reports
---

This Reposotory will be used to create NER Error reports for Enigma Legder and Statement data on a daily Basis.

---
### About the Repository

1. We divided the NER errors based on the entity class. A DataFrame is created having 3 columns viz. Error Type (Entity Class), List of SMS, Count of SMS.

2. The SMS list of each error type is passed through Lattice to detect patterns and form clusters.

3. For each error type we have taken top 100 Lattice patterns based on their Pattern Importance Score.

4. A New Lattice column is added to the above created DataFrame.

5. Each row in the lattice column contains a dictionary having pattern as keys, and the values as [count, cluster_id and 2 examples of the SMS]. The dictionary will contain atmost 100 such elements (patterns).

6. This DataFrame is exported as a .xls file and is stored in a s3 folder. The Excel file can be used to check and flag the SMS pattern where our model is making errors.

---

### How to Run the Code

	1. daily_fishnet_report.py and daily_ledger_report.py are two python files that wil be used to generate daily reports.
	2. In the Terminal run  
		2.1 $pip install -r requirements.txt
		2.2 $python daily_ledger_report.py -d [date in DD-MM-YYYY]
		2.3 $python daily_fishnet_report.py -d [date in DD-MM-YYYY]
	3. After execution the Excel files will be stored in the specified s3 folder.

### Example for 6th July, 2021 Ledger and Statement report

	1. $pip install -r requirements.txt
	2. $python daily_ledger_report.py -d 06-07-2021
	3. $python daily_fishnet_report.py -d 06-07-2021